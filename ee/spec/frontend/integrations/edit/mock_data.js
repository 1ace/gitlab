export const mockIntegrationProps = {
  id: 25,
  projectId: 1,
  operating: true,
  personalAccessTokensPath: '/path/to/personal/access/tokens',
  googleCloudArtifactRegistryProps: {
    artifactRegistryPath: '/path/to/artifact/registry',
  },
  sections: [],
  fields: [],
};
